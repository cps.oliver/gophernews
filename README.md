# GopherNews

GopherNews is a simple Google News reader for the Gopher protocol, heavily inspired by the 68k.news project.

You can browse GopherNews via the following URL in a client like `lynx`:

    gopher://gophernews.net:70/

Or via the GopherNews HTTP proxy at:

    http://gophernews.net

This project makes extensive use of `flask-gopher` by [Michael Lazar](https://mozz.us/), my personal thanks goes out to him for the creation of that flask extension.

## Screenshots

GopherNews on Netscape:

[![GopherNews Root Selector](images/root.GIF)](images/root.GIF)

GopherNews article listing on Netscape:

[![GopherNews Article listing](images/news_list.GIF)](images/news_list.GIF)

GopherNews on Netscape reading an article:

[![GopherNews Article](images/article.GIF)](images/article.GIF)

## Why?

I'm a big fan of the Gopher Protocol as it's simplistic, distraction free, and is extremely light weight.

Gopher is the perfect fit for browsing on very old computers or when you simply want a lighter experience than the web provides.

I think 68k.news is a great service, but I find even it a bit heavy for some of the older computers I own, so I decided to create an even lighter weight service. GopherNews is *not* intended to replace other news readers for vintage computers, it is simply another *option*.

## Blacklisted News Sources

Some news sources are blacklisted from this service. This isn't
politically motivated, any blacklisted news sources are blacklisted
for technological reasons such as requiring JS to render their
content, or being primarily video based. 

You can view a list of blacklisted sources and the reasons via the `/blacklist` 
selector.

## Running

```
pip3 install -r requirements.txt
./start.sh
```

## Roadmap

[x] Basic ability to read news for US and Canada

[x] Blacklisting of broken sources

[x] GopherNews.net setup and accepting requests

[X] Fixing of links within articles 
- I opted to remove them as they serve little use in the context of Gopher.

[ ] Stripping out filler in articles (eg. 'Subscribe' notices)

[ ] Support loading/downloading of images from news articles via new selector

[x] Support for 'interest' articles (eg. tech or health)

[x] Support for additional countries

## FAQ

+ Why not Gemini?
    + My primary use case for this is to read the news on very old computers. There is no practical way to support Gemini on very old computers due its TLS requirement, thus writing clients for said computers is effectively a nonstarter, at least for me.
    + I am not personally interested in Gemini.
    + I am not going to write off the idea of hosting a Gopher->Gemini proxy, but that is not a high priority right now. If you want to host one, let me know.

+ What clients have you tested?
    + Lynx, TurboGopher, Netscape, Kristall

+ GopherNews looks wrong and characters are jumbled, why?
    + Use a monospaced font, a few clients (eg. TurboGopher) don't default to one automatically.

+ The search selector doesn't work with spaces, why?
    + This is highly client dependant, some work with spaces as is, some work with encoded spaces, some outright don't work at all with spaces. I am really not sure right now how to solve this issue. If you absolutely can not use spaces, use dashes `-` instead.

+ Nothing is loading, what's up?
    + In the event of high traffic Google *may* rate limit GopherNews. I don't forsee this as being a serious issue given the limited traffic on Gopher, but keep it in mind.

+ I get an error five seconds after trying to load an article, why?
    + There is a five second timeout on retrieving articles from remote sources, if they don't return within this window it's unlikely they ever will and the request is killed.

+ An article looks like garbage, why?
    + I am working on improving the rendering of articles and removing noise from them, but this is a side effect of how modern online news articles are written. They are multimedia heavy and often have all kinds of irrelevant nonsense spread throughout them which causes issues when converting to plain text. Things will get better as I work on this project.
        + Some categories of news are worse than others about this. Gaming and Tech "news" in particular are very bad for this as a huge portion of those articles are actually advertorials.