BLACKLISTED_SOURCES = [
    {
        'url': 'https://www.nytimes.com',
        'reason': 'Requires JS to render content.'
    },
    {
        'url': 'https://www.youtube.com',
        'reason': 'No text based content.'
    },
    {
        'url': 'https://thehill.com',
        'reason': 'Strict rate limiting to the point of being unusable.'
    },
    {
        'url': 'https://www.newsweek.com',
        'reason': 'Blocks GopherNews/bots.'
    },
    {
        'url': 'https://www.sportsnet.ca',
        'reason': 'Requires JS to render content.'
    },
    {
        'url': 'https://www.thehockeynews.com',
        'reason': 'Requires JS to render content.'
    },
    {
        'url': 'https://canucksarmy.com',
        'reason': 'Requires JS to render content.'
    },
    {
        'url': 'https://www.miamiherald.com',
        'reason': 'Requires JS to render content/Blocks Gophernews/bots.'
    }
]

def is_blacklisted(source_url):
    is_url_blacklisted = any(d['url'] == source_url for d in BLACKLISTED_SOURCES)
    is_canada_blacklisted = source_url.endswith('.ca')
    return is_url_blacklisted or is_canada_blacklisted