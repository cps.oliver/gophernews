class GopherLineBuilder:
    def __init__(self):
        # Set the most sensible defaults, since info type will be most used
        self.line_type = 'i'
        self.line_display_string = ''
        self.line_selector = ''
        self.line_host = 'fake.com'
        self.line_port = '70'
        self.line = ''

    def type(self, type):
        self.type = type
        return self

    def display_string(self, display_string):
        self.line_display_string = display_string
        return self

    def selector(self, selector):
        self.line_selector = selector
        return self

    def host(self, host):
        self.line_host = host
        return self

    def port(self, port):
        self.line_port = port
        return self
    
    def build(self):
        self.line = '{}{}\t{}\t{}\t{}'.format(self.line_type, self.line_display_string, self.line_selector, self.line_host, self.line_port)
        return self

    def to_string(self):
        return self.line