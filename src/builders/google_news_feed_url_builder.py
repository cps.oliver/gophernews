FEED_URL_BASE = 'https://news.google.com/'

class GoogleNewsFeedUrlBuilder:
    def __init__(self):
        self.url = FEED_URL_BASE
        self.type = None

    def news(self):
        self.url = self.url + 'news/rss/headlines/section/topic/'
        self.type = 'news'
        return self

    def search(self):
        self.url = self.url + 'rss/search?q='
        self.type = 'search'
        return self

    def search_query(self, query):
        if type == 'news':
            raise Exception('News URL builder was initiated, can\'t use search specific option')

        self.url = self.url + query
        return self

    def topic(self, topic):
        if type == 'search':
            raise Exception('Search URL builder was initiated, can\'t use news specific option')

        self.url = self.url + topic.upper() + '?hl=en'
        return self

    def nation(self, nation):
        if type == 'search':
            raise Exception('Search URL builder was initiated, can\'t use news specific option')

        if nation is not None:
            self.url = self.url + '&ned=' + nation.upper()

        return self

    def build(self):
        if type == 'news':
            self.url = self.url + '?hl=en'

        if type == 'search':
            self.url = self.url + '&hl=en-US&ceid=US:en&gl=US'

        return self

    def to_string(self):
        return self.url