from unidecode import unidecode

WHITELIST = set('µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ')

def decode_text(str):
    return ''.join(c if c in WHITELIST else unidecode(c) for c in str)