import requests
from wrapt_timeout_decorator import *
from fake_useragent import UserAgent

ua = UserAgent()

# Some news sources behave badly and won't return data to us, so there is a maximum
# threshold of 10 seconds on any requests before an exception is raised.
@timeout(10)
def get_remote_resource_timed(url):
    response = requests.get(url)

    # The initial URL will cause at least one redirect to occur with the default User-Agent.
    # We can't use a custom UA because Google then assumes we have JS and relies on JS to redirect us.
    # We'll make the call to google first with the default requests UA, get the final destination and then request it
    # again with a legitimate UI to avoid them blocking us.
    # Not an amazing solution, but it's the best I've got for now.
    if response.history:        
        return requests.get(response.url, headers={
            'user-agent': ua.random
        })
    else:
        return response