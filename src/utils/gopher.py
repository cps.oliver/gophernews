def str_to_gopher_lines(str, line_type):
    return '\n'.join([line_type(line) for line in str.split('\n')])

def list_to_gopher_lines(list, line_type):
    return '\n'.join([line_type(line) for line in list])