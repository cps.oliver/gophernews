from bs4 import BeautifulSoup
from html2text import html2text
import re

COMMENT_PREFIXES = ('##', '#  ', '_-- ')
KNOWN_GARBAGE = [
    'This advertisement has not loaded yet, but your article continues\nbelow.',
    'Story continues below advertisement',
    'Descrease article font size',
    'Increase article font size'
]


def remove_external_content(article):
    html_doc = BeautifulSoup(article, features='lxml')

    # We don't need links, so we'll strip them out, but leave their text.
    for a in html_doc.findAll('a'):
        a.replace_with(a.contents[0])

    # We don't need images, so we'll strip them out completely.
    for img in html_doc.findAll('img'):
        img.replaceWithChildren()

    return str(html_doc)


def remove_trash(content):
    content = '\n'.join([line for line in content.splitlines() if not line.startswith(COMMENT_PREFIXES)])
    for item in KNOWN_GARBAGE:
        content = content.replace(item, '')
    content = re.sub(r'\n\s*\n', '\n\n', content)
    return content

def convert_html_to_text(article):
    return html2text(article, bodywidth=70)
