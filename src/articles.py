import feedparser
import textwrap
from readability import Document
from builders.google_news_feed_url_builder import GoogleNewsFeedUrlBuilder
from builders.gopher_line_builder import GopherLineBuilder
from utils.article import remove_external_content, convert_html_to_text, remove_trash
from utils.gopher import str_to_gopher_lines, list_to_gopher_lines
from utils.networking import get_remote_resource_timed
from utils.text import decode_text
from blacklist import is_blacklisted

class ArticleDetails:
    def __init__(self, title, link, pubDate):
        self.title = title
        self.selector = link
        self.pubDate = pubDate
        self.__process_title()
        self.__process_selector()

    def __process_title(self):
        title_lines = []
        
        for line in textwrap.wrap(self.title, width=70):
            title_lines.append(GopherLineBuilder().display_string(line).build().to_string())

        self.title = '\n'.join(title_lines)

    def __process_selector(self):
        # We must escape the selector in some capacity, because forward slashes are
        # seen as different directories. Gopher servers largely have no concept of URL params.
        self.selector = 'article/{}'.format(self.selector.replace('/', '&sl;'))


class ArticleContent:
    def __init__(self, title, url, body):
        self.title = title
        self.url = url
        self.body = body


def __get_article_list_from_rss_feed(url):
    feed = feedparser.parse(url)
    articles = []

    for entry in feed['entries']:
        if is_blacklisted(entry['source'].href):
            continue

        articles.append(ArticleDetails(entry['title'], entry['link'], entry['published']))

    return articles

def get_national_article_list(topic, nation=None):
    return __get_article_list_from_rss_feed((
        GoogleNewsFeedUrlBuilder()
            .news()
            .topic(topic)
            .nation(nation)
            .build()
            .to_string()
    ))

def get_search_article_list(search_term):
    return __get_article_list_from_rss_feed((
        GoogleNewsFeedUrlBuilder()
            .search()
            .search_query(search_term)
            .build()
            .to_string()
    ))

def get_article_content(gopher, article_url):
    response = get_remote_resource_timed(article_url)
    document = Document(response.text)

    # Title processing
    raw_title = decode_text(document.short_title())
    raw_title = textwrap.wrap(raw_title, width=70)
    title = list_to_gopher_lines(raw_title, gopher.menu.info)

    # URL processing
    article_url_line = gopher.menu.html('Source', article_url)

    # Content Processing
    raw_content = remove_external_content(document.summary())
    raw_content = convert_html_to_text(raw_content)
    raw_content = remove_trash(decode_text(raw_content))
    content = str_to_gopher_lines(raw_content, gopher.menu.info)

    return ArticleContent(title, article_url_line, content)