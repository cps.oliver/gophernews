#!/usr/bin/env python3
import logging
import os
from articles import get_national_article_list, get_article_content, get_search_article_list
from blacklist import BLACKLISTED_SOURCES
from flask import Flask, request, url_for
from flask_gopher import GopherExtension, GopherRequestHandler
from guestbook import Guestbook

VALID_NATIONS = ['US', 'UK', 'AU', 'NZ', 'DE', 'BR', 'FR']
VALID_TOPICS = ['business', 'technology', 'entertainment', 'sports', 'science', 'health']
GUESTBOOK_DIRECTORY = os.getcwd()

app = Flask(__name__, static_url_path='')
app.config.from_pyfile('config.cfg')
gopher = GopherExtension(app)
static_directory = gopher.serve_directory(app.static_folder, 'static_handler', show_timestamp=True)

logging.basicConfig(filename='gophernews.log', filemode='a', format='%(name)s - %(levelname)s - %(message)s')

@app.route('/')
def index():
    return gopher.render_menu_template('index.gopher')

@app.route('/')
@app.route('/<path:filename>')
def static_handler(filename=''):
    is_directory, file = static_directory.load_file(filename)
    if is_directory:
        return gopher.render_menu_template('error.gopher', message='Directory browsing is not allowed')
    else:
        return file

@app.route('/about')
def about():
    return gopher.render_menu_template('about.gopher')

@app.route('/blacklist')
def blacklist():
    return gopher.render_menu_template('blacklist.gopher', blacklist=BLACKLISTED_SOURCES)

@app.route('/guestbook')
def guestbook():
    return gopher.render_menu_template('guestbook.gopher', entries=Guestbook(GUESTBOOK_DIRECTORY).get_all())

@app.route('/guestbook_sign')
def guestbook_sign():
    message = request.environ['SEARCH_TEXT']
    try:
        Guestbook(GUESTBOOK_DIRECTORY).add(message)
        return gopher.render_menu_template('guestbook_signed.gopher')
    except Exception as e:
        logging.error('Error occurred signing guestbook: ' + str(e))
        return gopher.render_menu_template(
            'error.gopher', 
            message='Error encountered: ' + str(e)
        )

@app.route('/national', defaults={'country': None})
@app.route('/national/<country>')
def national_news_listing(country):
    if country not in VALID_NATIONS:
        logging.info('Invalid country provided: ' + country)
        return gopher.render_menu_template(
            'error.gopher', 
            message='Not a valid country'
        )

    return gopher.render_menu_template(
        'news_list.gopher', 
        articles=get_national_article_list('nation', country),
        host=app.config['SERVER_ADDRESS'],
        port=app.config['PORT']
    )

@app.route('/topic', defaults={'topic': None})
@app.route('/topic/<topic>')
def topic_news_listing(topic):
    if topic not in VALID_TOPICS:
        logging.info('Invalid topic provided: ' + topic)
        return gopher.render_menu_template(
            'error.gopher', 
            message='Not a valid topic'
        )

    return gopher.render_menu_template(
        'news_list.gopher', 
        articles=get_national_article_list(topic),
        host=app.config['SERVER_ADDRESS'],
        port=app.config['PORT']
    )

@app.route('/search')
def search_news():
    search_term = request.environ['SEARCH_TEXT']

    return gopher.render_menu_template(
        'news_list.gopher', 
        articles=get_search_article_list(search_term),
        host=app.config['SERVER_ADDRESS'],
        port=app.config['PORT']
    )

@app.route('/article', defaults={'url': None})
@app.route('/article/<url>')
def article_view(url):
    article_url = url.replace('&sl;', '/')
    
    try:
        article = get_article_content(gopher, article_url)

        return gopher.render_menu_template(
            'article.gopher', 
            title=article.title, 
            url=article.url,
            content=article.body
        )
    except Exception as e:
        logging.error('Error occurred viewing article: ' + str(e))
        return gopher.render_menu_template(
            'error.gopher', 
            message='Error encountered: ' + str(e)
        )

if __name__ == '__main__':
   app.run(
        host=app.config['HOST'],
        port=app.config['PORT'],
        threaded=app.config['THREADED'],
        processes=app.config['PROCESSES'],
        request_handler=GopherRequestHandler
    )
