import os
from datetime import datetime
from better_profanity import profanity
from builders.gopher_line_builder import GopherLineBuilder
import textwrap

GUESTBOOK_FILE_NAME = 'guestbook.txt'

class Guestbook:
    def __init__(self, base_path, file_name=GUESTBOOK_FILE_NAME):
        self.base_path = base_path
        self.guestbook_file_path = os.path.join(self.base_path, file_name)

    def get_all(self):
        entry_lines = []

        with open(self.guestbook_file_path, 'a+') as f:
            # We're opening in a+ so if the file doesn't exist it gets created.
            # As a consequence, we must seek to the very start of the file
            f.seek(0,0)

            for line in f.readlines():
                date, entry = line.split('\t')

                entry_line_wrap = []
                        
                for line in textwrap.wrap(entry, width=70):
                    entry_line_wrap.append(GopherLineBuilder().display_string(line).build().to_string())

                entry_lines.append({ 'date': date, 'entry': '\n'.join(entry_line_wrap).rstrip() })

        entry_lines.reverse()

        return entry_lines

    def add(self, entry):
        if entry == '':
            raise Exception('Entry was blank. This is likely a client issue.')

        if profanity.contains_profanity(entry):
            raise Exception('Profane entries are not allowed.')

        now = datetime.now()
        date_str = now.strftime('%B %d, %Y %H:%M:%S')
        cleaned_entry = entry.replace('\t', '')
        cleaned_entry = cleaned_entry.replace('\n', '')

        with open(self.guestbook_file_path, 'a+') as f:
            f.write('{}\t{}\n'.format(date_str, cleaned_entry))
